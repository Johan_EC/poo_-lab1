package javafxapplication3;

import java.util.ArrayList;

 /*
  Se crea la clase curso con todos los datos que debe contener */

public class Curso {
     
    public String escuela;
    public String profesor;
    public String horario;
    public int cupo;
    public String nombre;
    public String sede;
   
    /*
    Se crea este arraylist para agregarle al curso todos los estudiantes matrculados
     */ 
    
    public ArrayList<String> matriculados = new ArrayList<String>();

          
    /*
     Funciones para asignarles un valor a los atributos y retornar cada uno de sus valores
     */
    
    public void setProfesor(String p1){ this.profesor = p1;}
    public String getProfesor() { return profesor; }

    public void setEscuela(String ES) { this.escuela = ES;}
    public String getEscuela() { return escuela; }

    
    public void setNombre(String Nombre) { this.nombre = Nombre;}
    public String getNombre() { return nombre; }
    
    public void setCupo(int Cupo){this.cupo = Cupo;}
    public int getCupo() { return cupo; }

  
    public void setHorario(String Horario) { this.horario = Horario; }
    public String getHorario() { return horario; }

    public void setSede(String sede) { this.sede = sede; }
    public String getSede() { return sede; }

    /*
      Retorna un string conteniendo todos los atributos
    */

    @Override
    public String toString() {
        return "{" +
                ", Nombre='" + nombre + 
                ", Escuela = " + escuela +
                ", cupo=" + cupo +
                ", Sede='" + sede + 
                ", Profesor =" + profesor +
                ", Estudiantes='" + matriculados + 
                ", Horario='" + horario + 
                "}";
    }


}



