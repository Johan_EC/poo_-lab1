/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication3;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


/**
 * FXML Controller class
 *
 * @author PCPCPC
 */
public class FXMLDocumentController2 extends FXMLDocumentController implements Initializable {

    /**
     * 
     * 
     * Initializes the controller class.
     */
     @FXML
    TextField nombrefx;
     @FXML
    TextField carnefx;
     @FXML
    TextField contrafx;
     @FXML
    TextField cargof;
     @FXML
    Label  statusfx;
       
    @FXML
    private void signion(ActionEvent event) throws IOException {
        
        float  a = Float.parseFloat(contrafx.getText());
        float  b = Float.parseFloat(carnefx.getText());
        
        
        if(cargof.getText().equals("estudiante")){
                Estudiante es = new Estudiante();
                es.cargo = cargof.getText();
                es.nombre = nombrefx.getText();
                es.contraseña = a ;
                es.carne = b;
                registrarE(es);
                ingresarE(event);
        }
        if(cargof.getText().equals("profesor")){   
        
                Profesor pro = new Profesor();
                pro.cargo = cargof.getText();
                pro.nombre = nombrefx.getText();
                pro.contraseña = a ;
                pro.carne = b;
                registrarP(pro);
                ingresarPE(event);
        }
        
        if(cargof.getText().equals("administrador")){   
                Administrador ad = new Administrador();
                ad.cargo = cargof.getText();
                ad.nombre = nombrefx.getText();
                ad.contraseña = a ;
                ad.carne = b;
                registrarA(ad);
                ingresarA(event);
       }
        }
    
     @Override
    public void ingresarE(ActionEvent event) throws IOException{
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("FXMLEstudiante.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage)((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(home_page_scene);
        app_stage.hide();
        app_stage.show();
    }
   
     @Override
    public void ingresarPE(ActionEvent event) throws IOException{
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("FXMLProfesor.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage)((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(home_page_scene);
        app_stage.hide();
        app_stage.show();
    }
    
     @Override
    public void ingresarA(ActionEvent event) throws IOException{
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("FXMLDocument3.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage)((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(home_page_scene);
        app_stage.hide();
        app_stage.show();
    }
  
       
        
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
