package javafxapplication3;



public class Profesor extends Usuario{

    public Profesor(){
        super();
    }


    @Override
    public String toString() {
        return "{" +
                "nombre=" + nombre +
                ", carne='" + carne + '\'' +
                ", cargo=" + cargo +
                ", contraseña='" + contraseña + '\'' +
                '}';
    }
}
