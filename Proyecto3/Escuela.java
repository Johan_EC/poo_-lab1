package javafxapplication3;

public class Escuela{

    public String nombre;
    public String codigo;



    public void setNombre(String Nombre ) { this.nombre = Nombre; }
    public String getNombre() { return nombre; }

    public void setCodigo(String Codigo ) { this.codigo = Codigo; }
    public String getCodigo() { return codigo; }

    @Override
    public String toString() {
        return "{" +
                "nombre=" + nombre +
                ", codigo='" + codigo  + '\'' +
                '}';
    }

}
