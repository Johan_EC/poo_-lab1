/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication3;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 *
 * @author PCPCPC
 */
public class FXMLDocumentController implements Initializable {
      
    @FXML
    Label A;
            
    @FXML
    Button loginfx;

    @FXML
    Button signinfx;

    @FXML
    TextField passfx;

    @FXML
    TextField workfx;
    
    @FXML
    TextField idfx;

    @FXML
    ImageView TEC1;

    @FXML
    ImageView TEC2;

      @FXML
    TextField cargofx;
    
    
    @FXML
    private void login(ActionEvent event) throws IOException {
        
       if(workfx.getText().equals("") && idfx.getText().equals("")&& passfx.getText().equals("")){
           A.setText("Login failess");
           
       } 
        
        float  a = Float.parseFloat(idfx.getText());
        float  b = Float.parseFloat(passfx.getText());
       
       if(workfx.getText().equals("estudiante")){
          if(Plataforma.plataforma.ES != null){
            for(int i=0; i <= Plataforma.plataforma.ES.size();i++) {    
               if(Plataforma.plataforma.ES.get(i).getContraseña() == b && Plataforma.plataforma.ES.get(i).getCarne() == a ){
                   ingresarE(event);}}
            A.setText("Login failess");
            
       }}
       
       if(workfx.getText().equals("administrador")){
          if(Plataforma.plataforma.AD != null){
            for(int i=0; i <= Plataforma.plataforma.AD.size();i++) {    
               if(Plataforma.plataforma.AD.get(i).getContraseña() == b && Plataforma.plataforma.AD.get(i).getCarne() == a ){
                   ingresarA(event);}}
            A.setText("Login failess");
        
       }}
          
       if(workfx.getText().equals("profesor")){
          if(Plataforma.plataforma.PR != null){
            for(int i=0; i <= Plataforma.plataforma.PR.size();i++) {    
               if(Plataforma.plataforma.PR.get(i).getContraseña() == b && Plataforma.plataforma.PR.get(i).getCarne() == a ){
                   ingresarPE(event);}}
            A.setText("Login failess");
        
       }}
       }
     
   
    @FXML
    private void Signin(ActionEvent event) throws IOException {
        
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("FXMLDocument2.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage)((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(home_page_scene);
        app_stage.hide();
        app_stage.show();
        
    }
    
    
    

    public void Matricular(Estudiante estudiante, Curso curso){
        if(curso.cupo > 0){
          curso.matriculados.add(estudiante);
          curso.cupo--;
          estudiante.CM.add(curso);
        }
    }

    public void agregarCurso(Curso curso, ArrayList CU){

        CU.add(curso);
    }

    public void misCursos(ArrayList CM){

            Iterator<Curso> itrcurso = CM.iterator();
            while(itrcurso.hasNext()){
                Curso curso = itrcurso.next();
                    System.out.println(curso);
                }
    }
    
    public void ingresarE(ActionEvent event) throws IOException{
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("FXMLEstudiante.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage)((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(home_page_scene);
        app_stage.hide();
        app_stage.show();
    }
   
    public void ingresarPE(ActionEvent event) throws IOException{
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("FXMLProfesor.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage)((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(home_page_scene);
        app_stage.hide();
        app_stage.show();
    }
    
    public void ingresarA(ActionEvent event) throws IOException{
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("FXMLDocument3.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage)((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(home_page_scene);
        app_stage.hide();
        app_stage.show();
    }
    
    
    public static void registrarE(Estudiante es){

              Plataforma.plataforma.ES.add(es);
    }

    public static void registrarP(Profesor pr){
              Plataforma.plataforma.PR.add(pr);

    }
    public static void registrarA(Administrador ad){

              Plataforma.plataforma.AD.add(ad);
    }
    
    public static void crearES(String nombre,String codigo){
           Escuela escuela = new Escuela();
           escuela.nombre = nombre;
           escuela.codigo = codigo;
    }
       
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}

  
    

