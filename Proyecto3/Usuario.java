package javafxapplication3;

public class Usuario {
    //Estos van a ser los atributos de cada usuario del sistema 

    public float carne;
    public float contraseña;
    public String nombre;
    public String cargo;

    public Usuario(){

    }

    public void setCarne(float carne) { this.carne = carne;}
    public float getCarne() { return carne; }

    public void setContraseña(float contraseña ) { this.contraseña = contraseña; }
    public float getContraseña() { return contraseña; }

    public void setNombre(String nombre ) { this.nombre = nombre; }
    public String getNombre() { return nombre; }

    public void setCargo(String cargo ) { this.cargo = cargo; }
    public String getCargo() { return cargo; }
    

}
