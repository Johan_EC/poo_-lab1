package javafxapplication3;





public class Administrador extends Usuario {

   public Administrador(){
        super();
    }

    @Override
    public String toString() {
        return "{" +
                "nombre=" + nombre +
                ", carne='" + carne + '\'' +
                ", cargo=" + cargo +
                ", contraseña='" + contraseña + '\'' +
                '}';
    }
}