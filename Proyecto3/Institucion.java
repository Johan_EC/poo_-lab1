package javafxapplication3;

public class Institucion {

    private String nombre = "Instituto Tecnologico de Costa Rica";
    public static String sede;
    
    public void setNombre(String Nombre ) { this.nombre = Nombre; }
    public String getNombre() { return nombre; }
    public void setSede(String Sede ) { this.sede = Sede; }
    public static String getSede() { return sede; }

    public static Institucion I = new Institucion();

}
