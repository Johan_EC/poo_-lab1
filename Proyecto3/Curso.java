package javafxapplication3;

import java.util.ArrayList;

public class Curso {

    public Escuela escuela;
    public String horario;
    public float cupo;
    public String nombre;
    public Profesor profesor;
    public String sede;
    
    public ArrayList<Estudiante> matriculados = new ArrayList<Estudiante>();

    public void setNombre(String Nombre) { this.nombre = Nombre;}
    public String getNombre() { return nombre; }
    
   
    public void setCupo(Float Cupo){this.cupo = Cupo;}
    public float getCupo() { return cupo; }

    public void setProfesor(Profesor p1){ this.profesor = p1;}
    public Profesor getProfesor() { return profesor; }

    public void setEscuela(Escuela ES) { this.escuela = ES;}
    public Escuela getEscuela() { return escuela; }

    public void setHorario(String Horario) { this.horario = Horario; }
    public String getHorario() { return horario; }

    public void setSede(String sede) { this.sede = sede; }
    public String getSede() { return sede; }

    public void setMatriculados(ArrayList Estudiantes) { this.matriculados = Estudiantes; }
    public ArrayList getMatriculados() { return matriculados  ;}



    @Override
    public String toString() {
        return "Curso{" +
                "escuela=" + escuela +
                ", horario='" + horario + '\'' +
                ", cupo=" + cupo +
                ", nombre='" + nombre + '\'' +
                ", profesor=" + profesor +
                ", sede='" + sede + '\'' +
                '}';
    }


}



