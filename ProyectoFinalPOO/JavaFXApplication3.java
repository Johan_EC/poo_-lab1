
package javafxapplication3;
import static javafx.application.Application.launch;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/*
 Esta es la clase main y es la que inicia el sistema
 */

public class JavaFXApplication3 extends Application {
    
   
    /*
    Funcion que nos abre la ventana de inicio
    */
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    
    
    
 
    public static void main(String[] args) {
        launch(args);
    }
    
}
