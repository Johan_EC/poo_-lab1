package javafxapplication3;


 /*
  Se crea la clase profesor la cual extiende de usuario
 */
public class Profesor extends Usuario{

     /*
      Constructor, para poder utilizar los atributos y metodos de la super clase
      */
    public Profesor(){
        super();
    }
    String Escuela;
    
    public void setEscuela(String escuela) { this.Escuela = escuela ;}
    public String getEscuela() { return Escuela; }
    
     /*
      Retorna un string conteniendo todos los atributos
      */
    @Override
    public String toString() {
        return "{" +
                " nombre=" + nombre +
                ", carne=" + carne + 
                ", cargo=" + cargo +
                ", contraseña=" + contraseña +
                ", escuela=" + Escuela +
                "}";
    }
}
