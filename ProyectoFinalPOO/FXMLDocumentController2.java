
package javafxapplication3;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;


/*
 Se crea esta clase controladora con todos los elementos a utilizar en la primer ventana
 */

public class FXMLDocumentController2 extends FXMLDocumentController implements Initializable {

     @FXML
    TextField nombrefx;
     @FXML
    TextField carnefx;
     @FXML
    TextField contrafx;
     @FXML
    TextField cargof;
      @FXML
    TextField schoolfxx;
     @FXML
    Label  statusfx;
     
      /*
     Esta funcion se utiliza para poder registrarnos, recibe los 5 datos que debe tener un usuario y
     se debe especificar de que tipo es, para asi crear el objeto y pasarle los datos, luego agrega el objeto
     en el arraylist correspondiente(llamando a la funcion registrar) y tambien asi saber cual ventana nos debe 
     abrir, para eso llama a la funcion ingresar()
     
    */
    @FXML
    private void signion(ActionEvent event) throws IOException {
        
       int a = Integer.parseInt(contrafx.getText());
       int b = Integer.parseInt(carnefx.getText());
        
        
        if(cargof.getText().equals("estudiante")){
                Estudiante es = new Estudiante();
                es.cargo = cargof.getText();
                es.nombre = nombrefx.getText();
                es.contraseña = a ;
                es.carne = b;
                registrarE(es);
                ingresarE(event);
        }
        if(cargof.getText().equals("profesor")){   
        
                Profesor pro = new Profesor();
                pro.cargo = cargof.getText();
                pro.nombre = nombrefx.getText();
                pro.contraseña = a ;
                pro.carne = b;
                pro.Escuela = schoolfxx.getText();
                registrarP(pro);
                ingresarPE(event);
        }
        
        if(cargof.getText().equals("administrador")){   
                Administrador ad = new Administrador();
                ad.cargo = cargof.getText();
                ad.nombre = nombrefx.getText();
                ad.contraseña = a ;
                ad.carne = b;
                registrarA(ad);
                ingresarA(event);
       }
        }

    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
