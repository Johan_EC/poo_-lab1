package javafxapplication3;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/*
 Se crea esta clase controladora con todos los elementos a utilizar en la primer ventana
 */
public class FXMLDocumentController implements Initializable {
      
    @FXML
    Label A;
            
    @FXML
    Button loginfx;

    @FXML
    Button signinfx;

    @FXML
    TextField passfx;

    @FXML
    TextField workfx;
    
    @FXML
    TextField idfx;

    @FXML
    ImageView TEC1;

    @FXML
    ImageView TEC2;

      @FXML
    TextField cargofx;
     
 /*
 Funcion que sirve para cargar todos los datos del sistema, crea los profesores,estudiantes,administradores y
 los cursos
 */
    @FXML  
    public void cargar(){
    
        addPRO("Samanta Ramijan Carmiol","profesor","CA",1,1);
        addPRO("Samuel Valverde Sanchez","profesor","MA",2,2);
        addPRO("Félix Nuñez Venegas","profesor","MA",3,3);
        addPRO("Diego Munguía Molina","profesor","CA",4,4);
        addPRO("Jaime Guiterrez Alfaro","profesor","CA",5,5);
        addPRO("Juan Pablo Prendas Rojas","profesor","MA",6,6);
        addPRO("Arturo Vega Vazquez","profesor","MA",7,7);
         
        Administrador admin = new Administrador();
        admin.nombre = "Ingrid Amador Solano";
        admin.cargo = "administrador";
        admin.carne = 11578909 ;
        admin.contraseña = 4321;
        registrarA(admin);
        
        Estudiante es1 = new Estudiante();
        es1.nombre = "Johan Echeverría Carmona";
        es1.cargo = "estudiante";
        es1.carne=2019213824;
        es1.contraseña=1234;
        registrarE(es1); 
        
        Escuela e1 = new Escuela();
        Escuela e2 = new Escuela();
        Escuela e3 = new Escuela();
        e1.codigo = "CA";
        e1.nombre = "Ingeniería en Computación";
        e2.codigo = "MA";
        e2.nombre = "Matemática";
        e3.codigo = "E";
        e3.nombre = "Ingeniería Electrónica";
        Plataforma.plataforma.ESC.add(e1);
        Plataforma.plataforma.ESC.add(e2);
        Plataforma.plataforma.ESC.add(e3);
        
        addCU("Introducción a la Programación","Samanta Ramijan Carmiol","CA","Centro Académico de Alajuela","{\n" +
"          \"dia\":\"martes\",\n" +
"          \"inicio\":\"13:00\",\n" +
"          \"fin\":\"14:50\"\n" +
"        },\n" +
"        {\n" +
"          \"dia\":\"jueves\",\n" +
"          \"inicio\":\"13:00\",\n" +
"          \"fin\":\"14:50\"\n" +
"        }",20);
        
        addCU("Introducción a la Programación","Jaime Guiterrez Alfaro","CA","Centro Académico de Alajuela","{\n" +
"          \"dia\":\"martes\",\n" +
"          \"inicio\":\"13:00\",\n" +
"          \"fin\":\"14:50\"\n" +
"        },\n" +
"        {\n" +
"          \"dia\":\"jueves\",\n" +
"          \"inicio\":\"13:00\",\n" +
"          \"fin\":\"14:50\"\n" +
"        }",20);
          
         addCU("Taller Programación","Samanta Ramijan Carmiol","CA","Centro Académico de Alajuela","{\n" +
"          \"dia\":\"martes\",\n" +
"          \"inicio\":\"15:00\",\n" +
"          \"fin\":\"16:50\"\n" +
"        },\n" +
"        {\n" +
"          \"dia\":\"jueves\",\n" +
"          \"inicio\":\"15:00\",\n" +
"          \"fin\":\"16:50\"\n" +
"        }",20);
         
         addCU("Taller Programación","Jaime Guiterrez Alfaro","CA","Centro Académico de Alajuela","{\n" +
"          \"dia\":\"martes\",\n" +
"          \"inicio\":\"15:00\",\n" +
"          \"fin\":\"16:50\"\n" +
"        },\n" +
"        {\n" +
"          \"dia\":\"jueves\",\n" +
"          \"inicio\":\"15:00\",\n" +
"          \"fin\":\"16:50\"\n" +
"        }",20);
          
         addCU("Programación Orientada a Objetos","Samanta Ramijan Carmiol","CA","Centro Académico de Alajuela","{\n" +
"          \"dia\":\"lunes\",\n" +
"          \"inicio\":\"8:00\",\n" +
"          \"fin\":\"11:50\"\n" +
"        }",20);
          
         addCU("Computación y Sociedad","Jaime Guiterrez Alfaro","CA","Centro Académico de Alajuela","{\n" +
"          \"dia\":\"miércoles\",\n" +
"          \"inicio\":\"15:00\",\n" +
"          \"fin\":\"18:50\"\n" +
"        }",20);
         
          addCU("Diseño de Software","Diego Munguía Molina","CA","Centro Académico de Alajuela","{\n" +
"          \"dia\":\"viernes\",\n" +
"          \"inicio\":\"8:00\",\n" +
"          \"fin\":\"11:50\"\n" +
"        }",20);
          
           addCU("Análisis de Algoritmos","Diego Munguía Molina","CA","Centro Académico de Alajuela","{\n" +
"          \"dia\":\"miércoles\",\n" +
"          \"inicio\":\"13:00\",\n" +
"          \"fin\":\"16:50\"\n" +
"        }",20);
         
           addCU("Cálculo Superior","Félix Nuñez Venegas","MA","Campus Tecnológico Central Cartago","{\n" +
"          \"dia\":\"martes\",\n" +
"          \"inicio\":\"13:00\",\n" +
"          \"fin\":\"14:50\"\n" +
"        },\n" +
"        {\n" +
"          \"dia\":\"jueves\",\n" +
"          \"inicio\":\"13:00\",\n" +
"          \"fin\":\"14:50\"\n" +
"        }\n" +
"      ]\n" +
"    },",30);
         
           addCU("Matemática Discreta","Samuel Valverde Sanchez","MA","Centro Académico de Alajuela","        {\n" +
"          \"dia\":\"miércoles\",\n" +
"          \"inicio\":\"7:00\",\n" +
"          \"fin\":\"8:50\"\n" +
"        },\n" +
"        {\n" +
"          \"dia\":\"viernes\",\n" +
"          \"inicio\":\"7:00\",\n" +
"          \"fin\":\"8:50\"\n" +
"        }",30);
         
           addCU("Matemática General","Juan Pablo Prendas Rojas","MA","Centro Académico de Alajuela","{\n" +
"          \"dia\":\"martes\",\n" +
"          \"inicio\":\"7:00\",\n" +
"          \"fin\":\"8:50\"\n" +
"        },\n" +
"        {\n" +
"          \"dia\":\"jueves\",\n" +
"          \"inicio\":\"7:00\",\n" +
"          \"fin\":\"8:50\"\n" +
"        }",30);
         
           addCU("Calculo Diferencial e Integral","Arturo Vega Vazquez","MA","Centro Académico de Alajuela","{\n" +
"          \"dia\":\"miércoles\",\n" +
"          \"inicio\":\"7:00\",\n" +
"          \"fin\":\"8:50\"\n" +
"        },\n" +
"        {\n" +
"          \"dia\":\"viernes\",\n" +
"          \"inicio\":\"7:00\",\n" +
"          \"fin\":\"8:50\"\n" +
"        }",30);
         
           
           addCU("Laboratorio de Circuitos Eléctricos","Sin Profesor Asignado","E","Centro Académico de Alajuela","        {\n" +
"          \"dia\":\"martes\",\n" +
"          \"inicio\":\"7:00\",\n" +
"          \"fin\":\"8:50\"\n" +
"        }",30);
         
       
         
    }
       
    /*
    Para crear un curso y agregarlo al sistema, esta funcion recibe como parametro todo los datos que tiene que tener 
    un curso.
    */
    public void addCU(String Nombre, String profesor, String Escuela, String sede, String Horario, int cupo){
     Curso c1 = new Curso();
     c1.nombre = Nombre;
     c1.profesor = profesor;
     c1.escuela = Escuela;
     c1.sede = sede;
     c1.horario = Horario;
     c1.cupo = cupo;
     Plataforma.plataforma.CU.add(c1);
    }       
     
    /*
    Para crear y agregar un profesor al sistema, recibe como parametro todos los datos 
    que tiene que tener un profesor.
   */
    public void addPRO(String nombre, String cargo, String escuela, int carne, int contrase){
        Profesor p1 = new Profesor();
        p1.nombre = nombre;
        p1.cargo = cargo;
        p1.carne = carne;
        p1.contraseña = contrase;
        p1.Escuela = escuela;
        registrarP(p1);
    }
    
    /*
     Funcion para poder iniciar sesion en el sistema, se debe ingresar su carne, contrasena y
     se debe especificar si es un estudiante, profesor o administrador, para buscar en el arraylist
     correspondiente sus datos y poder asi verificar si se encuentra registrado o no, si lo encuentra
     llama a otra funcion que nos abre la ventana correspondiente al tipo de usuario.
 
    */
    
    @FXML
    private void login(ActionEvent event) throws IOException {
        
       if(workfx.getText().equals("") && idfx.getText().equals("")&& passfx.getText().equals("")){
           A.setText("Login failess");
           
       } 
        
        int  a = Integer.parseInt(idfx.getText());
        int  b = Integer.parseInt(passfx.getText());
       
       if(workfx.getText().equals("estudiante")){
          if(Plataforma.plataforma.ES != null){
            for(int i=0; i <= Plataforma.plataforma.ES.size();i++) {    
               if(Plataforma.plataforma.ES.get(i).getContraseña() == b && Plataforma.plataforma.ES.get(i).getCarne() == a ){
                   ingresarE(event);}}
            A.setText("Login failess");
            
       }}
       
       if(workfx.getText().equals("administrador")){
          if(Plataforma.plataforma.AD != null){
            for(int i=0; i <= Plataforma.plataforma.AD.size();i++) {    
               if(Plataforma.plataforma.AD.get(i).getContraseña() == b && Plataforma.plataforma.AD.get(i).getCarne() == a ){
                   ingresarA(event);}}
            A.setText("Login failess");
        
       }}
          
       if(workfx.getText().equals("profesor")){
          if(Plataforma.plataforma.PR != null){
            for(int i=0; i <= Plataforma.plataforma.PR.size();i++) {    
               if(Plataforma.plataforma.PR.get(i).getContraseña() == b && Plataforma.plataforma.PR.get(i).getCarne() == a ){
                   ingresarPE(event);}}
            A.setText("Login failess");
        
       }}
       }
     
    /*
    Esta funcion se utiliza para que nos abra otra ventana donde podremos registrarnos
    */
    @FXML
    private void Signin(ActionEvent event) throws IOException {
        
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("FXMLDocument2.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage)((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(home_page_scene);
        app_stage.hide();
        app_stage.show();
        
    }
    /*
     Abre la ventana del tipo objeto Estudiante
     */ 
    public void ingresarE(ActionEvent event) throws IOException{
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("FXMLEstudiante.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage)((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(home_page_scene);
        app_stage.hide();
        app_stage.show();
    }
    /*
     Abre la ventana del tipo objeto Profesor
     */ 
    public void ingresarPE(ActionEvent event) throws IOException{
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("FXMLProfesor.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage)((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(home_page_scene);
        app_stage.hide();
        app_stage.show();
    }
    /*
     Abre la ventana del tipo objeto Administrador
     */ 
    public void ingresarA(ActionEvent event) throws IOException{
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("FXMLDocument3.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage)((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(home_page_scene);
        app_stage.hide();
        app_stage.show();
    }
    
    /*
     Funcion que recibe de parametro un objeto de tipo Estudiante 
     y lo agrega al Arraylist de estudiantes
     */ 
    public static void registrarE(Estudiante es){

              Plataforma.plataforma.ES.add(es);
    }
    
    
    /*
     Funcion que recibe de parametro un objeto de tipo Profesor 
     y lo agrega al Arraylist de profesores
     */
    public static void registrarP(Profesor pr){
              Plataforma.plataforma.PR.add(pr);

    }
    
    
    /*
     Funcion que recibe de parametro un objeto de tipo administrador 
     y lo agrega al Arraylist de administradore
     */
    public static void registrarA(Administrador ad){
              Plataforma.plataforma.AD.add(ad);
    }
    
    
       
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}

  
    

