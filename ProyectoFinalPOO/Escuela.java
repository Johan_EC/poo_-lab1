package javafxapplication3;

/*Se crea la clase Escuela con todos los datos que debe contener */

public class Escuela{

    public String nombre;
    public String codigo;

    
     /*
     Funciones para asignarles un valor a los atributos y retornar cada uno de sus valores
     */

    public void setNombre(String Nombre ) { this.nombre = Nombre; }
    public String getNombre() { return nombre; }

    public void setCodigo(String Codigo ) { this.codigo = Codigo; }
    public String getCodigo() { return codigo; }

    
    /*
      Retorna un string conteniendo todos los atributos
      */
    
    @Override
    public String toString() {
        return "{" +
                "nombre=" + nombre +
                ", codigo='" + codigo  + '\'' +
                '}';
    }

}
