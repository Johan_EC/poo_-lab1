
package javafxapplication3;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

 /*
 Se crea clase controladora para la ventana del profesor
 */
public class FXMLProfesorController implements Initializable {

     /*
     * Elementos que tocan el codigo de esta ventana
     */
     @FXML
     TextArea Area3fx;
  
     @FXML
     TextField Pcursofx;
 
   
 /*
 Funcion para que el objeto profesor pueda ver los cursos qe debe impartir.
 Se recorre la lista de cursos, buscando dentro de cada curso si el nombre del profesor se encuentra,
 de ser asi, se le pasa el toString del curso al elemento TextArea.
 */
   @FXML
    public void verC(ActionEvent event){
        for(int i=0; i <= Plataforma.plataforma.CU.size();i++) {
            if(Plataforma.plataforma.CU.get(i).profesor.equals(Pcursofx.getText())){
               Area3fx.appendText(Plataforma.plataforma.CU.get(i).toString()+"\n");}    
        }}
    
    /*
     Funcion que permite al usuario profesor regresar a la ventana principal
    */ 
     @FXML
    private void salir(ActionEvent event) throws IOException {
    
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage)((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(home_page_scene);
        app_stage.hide();
        app_stage.show();
        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
