package javafxapplication3;

public class Usuario {
     /*
      Se crea la clase usuario con los siguientes atributos, los cuales van a tener
      el objeto profesor, estudiante y administrador
      */

    public int carne;
    public int contraseña;
    public String nombre;
    public String cargo;

    public Usuario(){

    }
    
     /*
     Funciones para asignarles un valor a los atributos y retornar cada uno de sus valores
     */
    public void setCarne(int carne) { this.carne = carne;}
    public int getCarne() { return carne; }

    public void setContraseña(int contraseña ) { this.contraseña = contraseña; }
    public int getContraseña() { return contraseña; }

    public void setNombre(String nombre ) { this.nombre = nombre; }
    public String getNombre() { return nombre; }

    public void setCargo(String cargo ) { this.cargo = cargo; }
    public String getCargo() { return cargo; }
    

}
