
package javafxapplication3;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

 /*
 Se crea clase controladora para la ventana del estudiante y con los elementos que se van a utilizar
en esta ventana
 */

public class FXMLEstudianteController implements Initializable {


     @FXML
     TextArea Area2fx;
     @FXML
     TextArea Area3fx;
     @FXML
     TextField Ncursofx;
     @FXML
     TextField Pcursofx;
     @FXML
     TextField Icursofx;
    
      /*
      Funcion para matricular estudiante, se pasa el nombre del curso y el nombre del profesor de este 
      curso, esto para buscar con un for en el arraylist de cursos, el curso que deseamos, despues de 
     encontralo se llama una funcion auxiliar y le pasamos el curso, este auxilir recibe tambien el id del
     estudiante para buscarlo en el arraylist de estudiantes, y luego asi agregar el estudiante, al
     arraylist de estudiantes matriculas que posee el curso.
      */
    @FXML
    public void Matricularc(ActionEvent event){
      
        for(int i=0; i <= Plataforma.plataforma.CU.size();i++) {
             if(Plataforma.plataforma.CU.get(i).nombre.equals(Ncursofx.getText()) && Plataforma.plataforma.CU.get(i).profesor.equals(Pcursofx.getText())){
                AuxMatricularc(Plataforma.plataforma.CU.get(i));}
              
             }
  
        }
    
     /*
     Auxiliar de la funcion Matricularc
     */
    public void AuxMatricularc(Curso c){
        int v = Integer.parseInt(Icursofx.getText());
        
        for(int k = 0; k <= Plataforma.plataforma.ES.size();k++){
          if(Plataforma.plataforma.ES.get(k).carne == v){
            c.cupo --;
            Plataforma.plataforma.ES.get(k).CM.add(c);
            c.matriculados.add(Plataforma.plataforma.ES.get(k).nombre);}}}
          
    
     /*
     Funcion para ver los estudiantes matriculados
     Se debe ingresar el id del estudiante, para buscarlo, al encontrarlo se llama 
    una funcion auxiliar a la que le pasamos el estudiante y esta otra funcion recorre el
    arraylist de curso mastriculados que posee el estudiante y los muestra en pantalla
     */
    @FXML
    public void vermatriculados(){
        int v = Integer.parseInt(Icursofx.getText());
        
        for(int k = 0; k <= Plataforma.plataforma.ES.size();k++){
          if(Plataforma.plataforma.ES.get(k).carne == v){
            auxvermatriculados(Plataforma.plataforma.ES.get(k));}}}
          

     /*
     Aux de vermatriculados
     */
    public void auxvermatriculados(Estudiante es){
        for(int z = 0; z <= es.CM.size();z++){
           Area3fx.appendText(es.CM.get(z).toString()+"\n");}
    
    }
 
     /*
     Funcion para poder ver los cursos disponibles, basicamente lo que hace es verificar en
     el arraylist de cursos, cual curso tiene el cupo mayor a 0 y lo muestra.
     */
    @FXML
    public void verCursosDisponibles(ActionEvent event){
        for(int i=0; i <= Plataforma.plataforma.CU.size();i++) {    
             if(Plataforma.plataforma.CU.get(i).cupo > 0){
               Area2fx.appendText(Plataforma.plataforma.CU.get(i).toString()+"\n");}
        
        }
    }
    
     /*
     Funcion que permite al usuario profesor regresar a la ventana principal
    */ 
     @FXML
    private void salir(ActionEvent event) throws IOException {
    
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage)((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(home_page_scene);
        app_stage.hide();
        app_stage.show();
        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        // TODO
    }    
    
}
