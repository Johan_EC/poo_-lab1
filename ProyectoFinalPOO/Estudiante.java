package javafxapplication3;

import java.util.ArrayList;

 /*
  Se crea la clase estudiente la cual extiende de usuario
 */

public class Estudiante extends Usuario {
     
     /*
     Se crea un arraylist de objetos cursos, para que cada objeto estudiante tenga uno,
     y poder agregarle al arraylist los cursos matriculados
     */
    public ArrayList<Curso> CM = new ArrayList<Curso>();
    
     /*
      Constructor, para poder utilizar los atributos y metodos de la super clase
      */
    public Estudiante(){
        super();
    }
    
     /*
      Retorna un string conteniendo todos los atributos
      */
    
    @Override
    public String toString() {
        return "{" +
                " nombre=" + nombre +
                ", carne='" + carne +
                ", cargo=" + cargo +
                ", contraseña='" + contraseña +
                ", cursos'" + CM +
                "}";
    }
}


   

    