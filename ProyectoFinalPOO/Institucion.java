package javafxapplication3;

/*
  Se crea la clase curso con todos los datos que debe contener */

public class Institucion {

    private String nombre = "Instituto Tecnologico de Costa Rica";
    public static String sede;
    
    
         
    /*
     Funciones para asignarles un valor a los atributos y retornar cada uno de sus valores
     */
    
    public void setNombre(String Nombre ) { this.nombre = Nombre; }
    public String getNombre() { return nombre; }
    public void setSede(String Sede ) { this.sede = Sede; }
    public static String getSede() { return sede; }

         
    /*
     Aca se inicializa la institucion
     */
    public static Institucion I = new Institucion();

}
