package javafxapplication3;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import static javafxapplication3.FXMLDocumentController2.registrarA;
import static javafxapplication3.FXMLDocumentController2.registrarE;
import static javafxapplication3.FXMLDocumentController2.registrarP;


/*
 Se crea esta clase controladora con todos los elementos a utilizar en la primer ventana
 */

public class FXMLDocumentController3 extends FXMLEstudianteController implements Initializable {

    @FXML
    TextField namefx;
    
    @FXML
    TextField schoolfx;
    
    @FXML
    TextField quotafx;
    
    @FXML
    TextField shedulefx;
    
    @FXML
    TextField teacherfx;
    
    @FXML
    TextField headfx;
    
    @FXML
    TextField nombresfx;
    
    @FXML
    TextField carnesfx;
    
    @FXML
    TextField contrasfx;
    
    @FXML
    TextField cargosfx;
    
    @FXML
    TextField cargosfx1;
    
    @FXML
    TextField snamefx;
    
    @FXML
    TextField scodigofx;
    
    @FXML
    TextField idmfx;
    
    @FXML
    TextField comfx;
    
     @FXML
    Label coursesfx;
     
     @FXML
    TextArea Areafx;
    
     @FXML
    TextArea Areafx1;
    
     @FXML
    TextArea Areafx11;
     
     /*
     Funcion para crear y agregar un objeto escuela al sistema, recibe 2 datos que se los pasa al
     objeto y agrega este mismo al arraylist de escuelas
     */
    
     @FXML
    public void addES(ActionEvent event){
           Escuela escuela = new Escuela();
           escuela.nombre = snamefx.getText();
           escuela.codigo = scodigofx.getText();
           Plataforma.plataforma.ESC.add(escuela);
    }
     
     /*
     Funcion para crear y agregar un objeto Curso al sistema, recibe 6 datos que se los pasa al
     objeto y agrega este mismo al arraylist de cursos.
     */
    
    @FXML
    public void addcourse(ActionEvent event){
        
        Curso c = new Curso();
        if(teacherfx.getText().equals("")){
            c.profesor = "Sin profesor asignado";  
        }else{c.profesor = teacherfx.getText();}
        
        c.nombre = namefx.getText();
        c.sede = headfx.getText();
        c.horario = shedulefx.getText();
        int  d  = Integer.parseInt(quotafx.getText());
        c.cupo = d;
        
        for(int i = 0; i <= Plataforma.plataforma.ESC.size(); i++){
           if(schoolfx.getText().equals(Plataforma.plataforma.ESC.get(i).codigo)){
            c.escuela = Plataforma.plataforma.ESC.get(i).nombre;
            Plataforma.plataforma.CU.add(c);
        }
        }
       
        
       }
   
   /*
     Funcion para crear y agregar un objeto Usuario de algun tipo al sistema, para eso 
    recibe 4 datos, uno de esos especificando de que tipo de usuario se va a registar,
   los otros datos se los pasa al objeto y agrega este mismo al arraylist de cursos.
     */
    
     @FXML
    public void adduserm(ActionEvent event) throws IOException {
       
        int  a = Integer.parseInt(contrasfx.getText());
        int  b = Integer.parseInt(carnesfx.getText());
        
         switch (cargosfx.getText()) {
             
            case "estudiante":
                Estudiante es = new Estudiante();
                es.cargo = cargosfx.getText();
                es.nombre = nombresfx.getText();
                es.contraseña = a ;
                es.carne = b;
                registrarE(es);
               
                
                break;
                
            case "profesor":
                Profesor pro = new Profesor();
                pro.cargo = cargosfx.getText();
                pro.nombre = nombresfx.getText();
                pro.contraseña = a ;
                pro.carne = b;
                pro.Escuela = cargosfx1.getText();
                registrarP(pro);
               
                
                break;
                
            default:
                Administrador ad = new Administrador();
                ad.cargo = cargosfx.getText();
                ad.nombre = nombresfx.getText();
                ad.contraseña = a ;
                ad.carne = b;
                registrarA(ad);
               
                
                break;
        }
       
    }
  
    /*
     Funcion para ver las escuelas que hay en el sistema
     */
            
     @FXML
    public void verescuelas(ActionEvent event){
        for(int i=0; i <= Plataforma.plataforma.ESC.size();i++) {    
               Areafx11.appendText(Plataforma.plataforma.ESC.get(i).toString()+"\n");}
        }
    /*
     Funcion para ver los profesores que hay en el sistema
     */
    @FXML
    public void verP(ActionEvent event){
       for(int i=0; i <= Plataforma.plataforma.PR.size();i++) {    
               Areafx1.appendText(Plataforma.plataforma.PR.get(i).toString()+"\n");}
    }
     
    /*
     Funcion para ver los administradores que hay en el sistema
     */
    @FXML
    public void verA(ActionEvent event){
       for(int i=0; i <= Plataforma.plataforma.AD.size();i++) {    
               Areafx1.appendText(Plataforma.plataforma.AD.get(i).toString()+"\n");}
    }
        
    /*
     Funcion para ver las estudiantes que hay en el sistema
     */
    @FXML
    public void verE(ActionEvent event){
        for(int i=0; i <= Plataforma.plataforma.ES.size();i++) {    
               Areafx1.appendText(Plataforma.plataforma.ES.get(i).toString()+"\n");}
        
        }
    
    /*
     Funcion para ver los cursos que hay en el sistema
     */
     @FXML
    public void verC(ActionEvent event){
        for(int i=0; i <= Plataforma.plataforma.CU.size();i++) {    
               Areafx.appendText(Plataforma.plataforma.CU.get(i).toString()+"\n");}
    }
    
    /*
     Funcion para regresar a la ventana de inicio
     */
     @FXML
    private void salir(ActionEvent event) throws IOException {
    
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage)((Node) event.getSource()).getScene().getWindow();
        app_stage.setScene(home_page_scene);
        app_stage.hide();
        app_stage.show();
        
    }
    
    
           
     
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
