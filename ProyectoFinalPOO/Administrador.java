package javafxapplication3;



 /*
  Se crea la clase Administrador la cual extiende de usuario
 */

public class Administrador extends Usuario {

   /*
   Constructor, para poder utilizar los atributos y metodos de la super clase
   */
    
   public Administrador(){
        super();
    }
   
   
    /*
      Retorna un string conteniendo todos los atributos
      */
    @Override
    public String toString() {
        return "{" +
                "nombre =" + nombre +
                ", carne =" + carne + 
                ", cargo =" + cargo +
                ", contraseña =" + contraseña +
                "}";
    }
}